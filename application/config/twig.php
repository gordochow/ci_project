<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    $config = array(
                'view_path' => APPPATH.'views/',
                'cache_status' => true,
                'cache_lifetime'=> 3600,
                'cache' => APPPATH.'cache/views/',
                'auto_reload' => true
            );
