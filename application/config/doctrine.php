<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    $config = array(
        'models_path' => APPPATH.'models',
        'accessor_override' => true,
        'default_column_opts'=> array('notnull' => true, 'unsigned' => true),
        'default_id_opts' => array('name' => 'id', 'type' => 'integer', 'length' => 4),
    );

