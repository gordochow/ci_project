<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// Load Doctrine_Core class
require_once APPPATH.'libraries/Doctrine/Core.php';
// Autoload all Doctrine classes
spl_autoload_register(array('Doctrine_Core', 'autoload'));

class CI_Doctrine {

    private $ci;
    private $connections;
    private $options;

    public function __construct()
    {
        $this->ci = &get_instance();
        $this->ci->config->load('doctrine', true);
        //$this->ci->config->load('database', true);

        // Load Database configurations
        include_once APPPATH.'config/database.php';
        $this->connections  = $db;
        $this->options      = $this->ci->config->item('doctrine');

        $this->start();
    }

    public function connection()
    {
        return $this->connections;
    }

    public function generateModels($conn = 'default', $dir = null, $opts = array())
    {
        $dir = $dir ? $dir : $this->options['models_path'];
        Doctrine_Core::generateModelsFromDb($dir, array($conn), $opts);

        return $this;
    }

    public function start()
    {
        // we load our database connections into Doctrine_Manager
        // this loop allows us to use multiple connections later on
        foreach ($this->connections as $name => $values) {

            // first we must convert to dsn format
            $dsn = $this->connections[$name]['dbdriver'] .
                '://' . $this->connections[$name]['username'] .
                ':' . $this->connections[$name]['password'].
                '@' . $this->connections[$name]['hostname'] .
                '/' . $this->connections[$name]['database'];

            $this->connections[$name] = Doctrine_Manager::connection($dsn,$name);
        }

        $this->prepare();
    }

    public function prepare()
    {
        // telling Doctrine where our models are located
        Doctrine_Core::loadModels($this->options['models_path']);

        // this will allow us to use "mutators"
        Doctrine_Manager::getInstance()->setAttribute(
            Doctrine_Core::ATTR_AUTO_ACCESSOR_OVERRIDE, $this->options['accessor_override']);

        // this sets all table columns to notnull and unsigned (for ints) by default
        Doctrine_Manager::getInstance()->setAttribute(
            Doctrine_Core::ATTR_DEFAULT_COLUMN_OPTIONS,
            $this->options['default_column_opts']);

        // set the default primary key to be named 'id', integer, 4 bytes
        Doctrine_Manager::getInstance()->setAttribute(
            Doctrine_Core::ATTR_DEFAULT_IDENTIFIER_OPTIONS,
            $this->options['default_id_opts']);
    }
}
