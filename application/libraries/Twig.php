<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    require_once(APPPATH.'libraries/Twig/Autoloader.php');

    class CI_Twig {

        private $ci;
        protected $loader;
        protected $twig;

        private $view_path = 'application/views';
        private $options   = array(
            'cache' => 'application/cache/views/',
            'auto_reload' => true,
        );

        public function __construct()
        {
            $this->ci = &get_instance();
            $this->ci->config->load('twig', true);

            $this->options    = $this->ci->config->item('twig');
            $this->view_path  = $this->options['view_path'];
            $this->functions  = get_defined_functions();

            // Twig Templating System
            Twig_Autoloader::register();
            $this->loader = new Twig_Loader_Filesystem($this->view_path);
            $this->twig   = new Twig_Environment($this->loader, $this->options);

            // Loading Twig Configurations
            $this->twig->addGlobal('app', $this->ci);

            foreach ($this->functions['user'] as $key => $function) {
                $this->twig->addFunction($function, new Twig_Function_Function($function));
            }
        }

        public function view($template = null, $data = array())
        {
            if (!$template) {
                trigger_error("No template to render");
            }

            $template = $template.".php.twig";

            try {
                echo $this->twig->loadTemplate($template)->render($data);

            } catch (Exception $e) {
                trigger_error("Twig view error: ".$e->getMessage());
            }
        }

        public function addFunction($function)
        {
            if (function_exists($function)) {
                $this->twig->addFunction($function, new Twig_Function_Function($function));
            }
        }
    }
