<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Acceso extends CI_Controller {

    /**
     * Index Page for this controller.
     */
    public function index()
    {
        $data['styles']  = array(
            'packages/AdminLTE/plugins/iCheck/square/blue.css',
        );
        $data['scripts'] = array(
            'packages/AdminLTE/plugins/iCheck/icheck.min.js',
        );

        $data['login'] = $this->input->post();
//        $data['rol'] = Doctrine_Core::getTable('rol')->findOneBy('rol', 'REGISTRO');
//        $user = new User();
//        $user->id_usu = 1;
//        $user->link('Rol', array($rol->id_rol));
//        $user->usuario = 'foo';
//        $user->clave = '1234567u';
//        $user->apellidos_nombres = 'Bar Quaz';
//        $user->save();

        $data['title'] = "Acceso al Sistema";
        $this->twig->view('acceso/login', $data);
    }

    public function logout()
    {
        $data['person'] = array(
            'name' => 'Nick Bryan',
            'lastname' => 'Palomino Perez',
        );

        $data['elems'] = array(
            1,2,3,4,5,6,7,8
        );

        $data['title']  = "Supervisión";
        $data['content'] = "login/home";

        $this->twig->view('base/empty', $data);
    }
}
