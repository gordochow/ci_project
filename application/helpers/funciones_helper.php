<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');


if(!function_exists('replace_texto')){
    function replace_texto($cadena){
        return str_replace('-',' ',$cadena);
    }
}

if(!function_exists('url_amigable')){

    function url_amigable($url) {
      $urlMinuscula = strtolower($url);
      $buscarLetra = array('á', 'é', 'í', 'ó', 'ú', 'ñ');
      $reemplazar = array('a', 'e', 'i', 'o', 'u', 'n');
      $urlNueva = str_replace($buscarLetra, $reemplazar, $urlMinuscula);
      $buscarCaracteres = array(' ', '&', '\r\n', '\n', '+');
      $urlAceptable = str_replace($buscarCaracteres, '-', $urlNueva);
      $find = array('/[^a-z0-9\-<>]/', '/[\-]+/', '/<[^>]*>/');
      $repl = array('', '-', '');
      $urlRetornar = preg_replace($find, $repl, $urlAceptable);
      return $urlRetornar;
    }
}


if (!function_exists('convertir_mes_letras')) {

    function convertir_mes_letras($mes) {
        switch ($mes) {
            case '1':
                $texto = "Enero";
                break;
            case '2':
                $texto = "Febrero";
                break;
            case '3':
                $texto = "Marzo";
                break;
            case '4':
                $texto = "Abril";
                break;
            case '5':
                $texto = "Mayo";
                break;
            case '6':
                $texto = "Junio";
                break;
            case '7':
                $texto = "Julio";
                break;
            case '8':
                $texto = "Agosto";
                break;
            case '9':
                $texto = "Setiembre";
                break;
            case '10':
                $texto = "Octubre";
                break;
            case '11':
                $texto = "Noviembre";
                break;
            case '12':
                $texto = "Diciembre";
                break;
        }
        return $texto;
    }
}

if (!function_exists('tipo_postulante')) {

    function tipo_postulante($tipo) {
        if ($tipo == 'T') {
            $res = "TITULAR";
        } else {
            $res = "RESERVA";
        }
        return $res;
    }

}

if (!function_exists('array_column')) {

    function array_column(array $input, $columnKey, $indexKey = null) {
        $array = array();
        foreach ($input as $value) {
            if ( ! isset($value[$columnKey])) {
                trigger_error("Key \"$columnKey\" does not exist in array");
                return false;
            }
            if (is_null($indexKey)) {
                $array[] = $value[$columnKey];
            }
            else {
                if ( ! isset($value[$indexKey])) {
                    trigger_error("Key \"$indexKey\" does not exist in array");
                    return false;
                }
                if ( ! is_scalar($value[$indexKey])) {
                    trigger_error("Key \"$indexKey\" does not contain scalar value");
                    return false;
                }
                $array[$value[$indexKey]] = $value[$columnKey];
            }
        }
        return $array;
    }

}

if (!function_exists('array_suma_index')) {

    function array_suma_index($array, $campo)
    {
        return array_sum(array_column($array, $campo));
    }
}

if (!function_exists('verificar_estado')) {

    function verificar_estado($estado) {
        if ($estado == 1) {
            $res = "<button class='btn btn-flat bg-green'><i class='fa fa-check'></i></button> ";
        } else {
            $res = "<button class='btn btn-flat bg-red'><i class='fa fa-ban'></i></button>";
        }
        return $res;
    }

}

if (!function_exists('verificar_estado_imprenta')) {

    function verificar_estado_imprenta($estado) {
        if ($estado == 1) {
            $res = "<i class='fa fa-check fa-2x text-success'></i> ";
        } else {
            $res = "<i class='fa fa-circle fa-2x text-danger'></i>";
        }
        return $res;
    }

}

if(!function_exists('tiempo_momento_js')){

   function tiempo_momento_js($fecha){
      return '<script>document.write(moment("'.$fecha.'","YYYYMMDD").fromNow());console.log("'.$fecha.'");</script>';
   }
}

if (!function_exists('notildes')) {

    function notildes($nombre) {
        $find = array('Á', 'É', 'Í', 'Ó', 'Ú', 'Ñ', 'á', 'é', 'í', 'ó', 'ú', 'ñ');
        $repl = array('A', 'E', 'I', 'O', 'U', 'N', 'a', 'e', 'i', 'o', 'u', 'n');
        $nombreLimpio = str_replace($find, $repl, $nombre);
        return $nombreLimpio;
    }

}

if (!function_exists('tildesMayuscula')) {

    function tildesMayuscula($nombre) {
        $find = array('á', 'é', 'í', 'ó', 'ú', 'ñ');
        $repl = array('Á', 'É', 'Í', 'Ó', 'Ú', 'Ñ');
        $nombreLimpio = str_replace($find, $repl, $nombre);
        return $nombreLimpio;
    }

}


if (!function_exists('cerosIzquierda')) {

    function cerosIzquierda($valor, $longitud) {
        $res = str_pad($valor, $longitud, '0', STR_PAD_LEFT);
        return $res;
    }

}

if (!function_exists('utf8_mayusculas')) {

    function utf8_mayusculas($item) {
        return strtoupper(utf8_decode(trim($item)));
    }
}

if (!function_exists('utf8_minusculas')) {

    function utf8_minusculas($item) {
        return strtolower(utf8_decode(trim($item)));
    }
}


if (!function_exists('limpia_datos')) {

    function limpia_datos($params, $callback = 'utf8_mayusculas') {
        return array_map($callback, html_escape($params));
    }
}

if (!function_exists('si_no')) {
    // Recibe var como variable a evaluar
    // Recibe resp como valor a mostrar en caso este nulo
    // Recibe data como valores a evaluar y el texto que se muestra
    function si_no($var, $resp = '', $data = array()) {

        if (empty($data)) {
            $data = array(
                'si'=>array('1', 'SI'),
                'no'=>array('0', 'NO')
            );
        }

        $resp = ($var === $data['si'][0]) ? $data['si'][1] : $resp;
        $resp = ($var === $data['no'][0]) ? $data['no'][1] : $resp;

        return $resp;
    }
}

// /*if (!function_exists('nullCero')) {
//
//     function nullCero($valor) {
//         $res = ($valor == NULL) ? 0 : $valor;
//         return $res;
//     }
//
// }

if (!function_exists('nullCeroPorcentaje')) {

    function nullCeroPorcentaje($valor) {
        $res = ($valor == NULL) ? number_format(0.00, 2) : $valor;
        return $res;
    }

}

if (!function_exists('estado_asistencia')){
 function estado_asistencia($estado){
   if($estado=='NO'){
     $estado_mensaje = '<span class="btn bg-red">NO</span>';
   }else{
     $estado_mensaje = '<span class="btn bg-green">SI</span>';
   }
   return $estado_mensaje;
 }
}

//Añade dinamicamente archivos personalizados js en el pie de la pagina
if (!function_exists('add_js')) {

    function add_js($file = '') {
        $str = '';
        $ci = &get_instance();
        $header_js = $ci->config->item('header_js');

        if (empty($file)) {
            return;
        }

        if (is_array($file)) {
           if (!is_array($file) && count($file) <= 0) {
              return;
           }
        //    $file = array_merge($header_js, $file);

           foreach ($file AS $item) {

              $array_header_js[] = $item;
           }
        } else {

           $array_header_js[] = $file;
        }

        $ci->config->set_item('header_js', array_merge($header_js, $array_header_js));
    }
}

//Añade dinamicamente archivos js en el pie de la pagina
if (!function_exists('put_headersJs')) {

    function put_headersJs() {
        $str = '';
        $ci = &get_instance();
        $header_js = $ci->config->item('header_js');

        foreach ($header_js AS $item) {

            $item_path = base_url() . 'assets/js/' . $item;

            if (substr($item,0,2) === '//') {
                $item_path = $item;
            }

            $str .= '<script src="' . $item_path  . '.js"></script>' . "\n";
        }

        return $str;
    }

}

//añadir js al inicio tamare
if (!function_exists('add_js_')) {

   function add_js_($file = '') {
      $str  = '';
      $ci   = &get_instance();
      $header_js_ = $ci->config->item('header_js_');

      if (empty($file)) {
         return;
      }

      if (is_array($file)) {
         if (!is_array($file) && count($file) <= 0) {
            return;
         }

         foreach ($file AS $item) {

            $array_footer_js[] = $item;
         }
      } else {
          $array_footer_js[] = $file;
      }

      $ci->config->set_item('header_js_', array_merge($header_js_, $array_footer_js));
   }

}

if (!function_exists('put_headersJs_')) {

    function put_headersJs_() {
        $str = '';
        $ci = &get_instance();
        $header_js_ = $ci->config->item('header_js_');

        if (empty($header_js_)) {
            return;
        }

        foreach ($header_js_ AS $item) {

            $item_path = base_url() . 'assets/js/' . $item;

            if (substr($item,0,2) === '//') {
                $item_path = $item;
            }

            $str .= '<script src="' . $item_path  . '.js"></script>' . "\n";
        }

        return $str;
    }

}

//Añade dinamicamente archivos personalizados css en el header de la pagina
if (!function_exists('add_css')) {

    function add_css($file = '') {
        $str = '';
        $ci = &get_instance();
        $header_css = $ci->config->item('header_css');

        if (empty($file)) {
            return;
        }

        if (is_array($file)) {
            if (!is_array($file) && count($file) <= 0) {
                return;
            }

            foreach ($file AS $item) {

                $array_header_css[] = $item;
            }
        } else {

            $array_header_css[] = $file;
        }

        $ci->config->set_item('header_css',  array_merge($header_css, $array_header_css));
    }

}

//Añade dinamicamente archivos css en el header de la pagina
if (!function_exists('put_headersCss')) {

    function put_headersCss() {
        $str = '';
        $ci = &get_instance();
        $header_css = $ci->config->item('header_css');

        foreach ($header_css AS $item) {
            $ruta_path = base_url() . 'assets/css/' . $item;

            if (substr($item,0,2) === '//') {
                $ruta_path = $item;
            }

            $str .= '<link rel="stylesheet" href="' . $ruta_path . '.css"  />' . "\n";
        }

        return $str;
    }

}

if (!function_exists('assets')) {

    function assets($path) {
        $ruta_path = '';

        // $explode_path = explode('.', $path);
        // $length       = count($explode_path) - 1;
        //
        // if ($explode_path[$length] === 'css') {
        //     $ruta_path = base_url() . 'public/' . $path;
        // }
        //
        // if ($explode_path[$length] === 'js') {
        //     $ruta_path = base_url() . 'public/' . $path;
        // }

        if (substr($path, 0, 2) === '//') {
            $ruta_path = $path;
        } else {
            $ruta_path = base_url() . 'public/' . $path;
        }

        return $ruta_path;
    }

}

if (!function_exists('porcentaje')) {
    function porcentaje($dividendo, $divisor) {
        $res = 0;

        if ($divisor != 0 && $dividendo != 0) {
            $res = 100/($dividendo/$divisor);
        }

        return number_format($res, 1);
    }
}

if (!function_exists('dump')) {
    function dump($var) {
        // if (!$var) {
        //     return;
        // }
        echo "<pre>";
        print_r($var);
        echo "</pre>";
    }
}
