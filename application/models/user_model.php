<?php

class User extends Doctrine_Record {

    public function setTableDefinition()
    {
        $this->setTableName('usuario');
        $this->hasColumn('id_usu as id', 'integer', 4, array(
            'type' => 'integer',
            'length' => '4',
            'fixed' => false,
            'unsigned' => true,
            'primary' => true,
            'autoincrement' => true,
        ));
        $this->hasColumn('id_rol', 'integer', 4);
        $this->hasColumn('id_cargo', 'integer', 4);
        $this->hasColumn('usuario', 'string', 8);
        $this->hasColumn('clave', 'string', 20);
        $this->hasColumn('apellidos_nombres', 'string', 80);
        $this->hasColumn('dni', 'string', 8);
        $this->hasColumn('nro_brigada', 'integer', 4);
        $this->hasColumn('estado', 'boolean');
    }

    public function setUp()
    {
        $this->hasMany('Rol', array(
            'local' => 'id_rol',
            'foreign' => 'id_rol',
        ));
    }
}