<?php

class Rol extends Doctrine_Record {

    public function setTableDefinition()
    {
        $this->setTableName('rol');
        $this->hasColumn('id_rol as id', 'integer', 4, array(
            'type' => 'integer',
            'length' => '4',
            'fixed' => false,
            'unsigned' => true,
            'primary' => true,
            'autoincrement' => true,
        ));
        $this->hasColumn('rol', 'string', 255);
        $this->hasColumn('estado', 'boolean');
    }


}